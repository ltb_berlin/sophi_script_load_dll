// helloWorldExe.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include "stdafx.h"
#include "stdio.h"
#include <iostream>
#include "showDialog.h"

#pragma comment(lib,"helloWorldDll.lib")

int _tmain(int argc, _TCHAR* argv[])
{
	printf("Hello, I'm a test app. What's your name?\n");
	wchar_t name[100] = {};
	std::wcin >> name;
	if (sayHelloPersonally(name)) {
		printf("The answer was yes\n");
	}
	else
	{
		printf("The answer was no\n");
	}
	return 0;
}

