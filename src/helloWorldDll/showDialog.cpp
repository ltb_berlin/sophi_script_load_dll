#define DLL_EXPORT

#include "showDialog.h"
#include <string>
#include <windows.h>
#include <wchar.h>

extern "C"
{
	DECLDIR bool sayHello(void)
	{
		return MessageBox(NULL, L"Hello world", L"sayHello", MB_YESNO) == IDYES;
	}

	DECLDIR bool sayHelloPersonally(const wchar_t *name)
	{
		bool result = false;
		std::wstring helloStr = L"Hello " + std::wstring(name);
		result = MessageBox(NULL, helloStr.c_str(), L"sayHelloPersonally", MB_YESNO) == IDYES;
		return result;
	}
}