#ifndef _showDialog_H_
#define _showDialog_H_

#if defined DLL_EXPORT
#define DECLDIR __declspec(dllexport)
#else
#define DECLDIR __declspec(dllimport)
#endif

extern "C"
{
	DECLDIR bool sayHello(void);
	DECLDIR bool sayHelloPersonally(const wchar_t *name);
}

#endif